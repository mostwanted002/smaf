// Placeholer main.go for now
package main

import (
	"fmt"
	"os"

	"github.com/akamensky/argparse"
	"gitlab.com/mostwanted002/smaf/models"
	"gitlab.com/mostwanted002/smaf/modules"
	"gitlab.com/mostwanted002/smaf/repository"
)

func main() {
	parser := argparse.NewParser("SMAF", "")

	path := parser.FilePositional(os.O_RDONLY, 0600, &argparse.Options{Required: true, Help: "Path to the file to be analyzed"})
	r := parser.String("r", "db-host", &argparse.Options{Required: true, Help: "MongoDB Host"})
	u := parser.String("u", "user", &argparse.Options{Required: true, Help: "MongoDB User"})
	p := parser.String("p", "pass", &argparse.Options{Required: true, Help: "MongoDB Password"})
	err := parser.Parse(os.Args)
	if err != nil {
		panic(err)
	}
	test := modules.Identifier(path)
	fmt.Printf("The file type identified as : %s\n", test.SampleTypeValue)
	fmt.Printf("SHA256: %s\n", test.Sha256sum)
	fmt.Printf("MD5: %s\n", test.Md5sum)
	//	if test.SampleTypeValue != "" {
	//	test.IsReported = modules.IsReported(&test)
	//		fmt.Printf("Has been reported: %t\n", test.IsReported)
	//	}
	fmt.Printf("Timestamp: %s\n", test.Timestamp)
	client := repository.InitiateConnection(models.DBCredentials{
		Host:     *r,
		Username: *u,
		Password: *p,
	})
	repository.PushToDB(test, client)
}
