package models

type Sample struct {
	Sha256sum       string
	Md5sum          string
	SampleType      string
	SampleTypeValue string
	SampleSize      int64
	IsReported      bool
	ReportURLs      []string
	Timestamp       string
}

type DBCredentials struct {
	Username string
	Password string
	Host     string
}

type FileInfo struct {
	Context string
	Value   string
}

type YARARule struct {
	RuleName    string
	Author      string
	Description string
	Reference   string
}

type TriageSignature struct {
	Signature string
	Score     string
}

type MalwareBazaarQueryResponse struct {
	Query_status string
	Data         []struct {
		sha256_hash    string
		sha3_384_hash  string
		sha1_hash      string
		md5_hash       string
		first_seen     string
		last_seen      string
		file_name      string
		file_size      int64
		file_type_mime string
		file_type      string
		reporter       string
		origin_country string
		anonymous      bool
		signature      string
		imphash        string
		tlsh           string
		telfhash       string
		gimphash       string
		ssdeep         string
		dhash_icon     string
		comments       string
		tags           []string
		code_sign      struct {
			subject_cn    string
			issuer_cn     string
			algorithm     string
			valid_from    string
			valid_to      string
			serial_number string
			cscb_listed   string
			cscb_reason   string
		}
		delivery_method string
		intelligence    struct {
			clamav    []string
			downloads string
			uploads   string
			mail      string
		}
		file_information []FileInfo
		ole_information  []string
		yara_rules       []YARARule

		Vendor_intel struct {
			ANYRUN struct {
				Malware_family string
				Verdict        string
				Date           string
				Analysis_URL   string
				Tags           []string
			}
			CERTPL_MWDB struct {
				Detection string
				Link      string
			}
			YOROI_YOMI struct {
				Detection string
				Score     string
			}
			vxCube struct {
				Verdict       string
				Maliciousness string
				Behaviour     []struct {
					Threat_level string
					Rule         string
				}
			}
			CAPE       struct{}
			DocGuard   struct{}
			FileScanIO struct {
				Verdict     string
				ThreatLevel string
				Confidence  string
				Report_link string
			}
			InQuestLabs struct {
				Verdict string
				URL     string
				Details []struct {
					Category    string
					Title       string
					Description string
				}
			}
			Intezer struct {
				Verdict      string
				Family_name  string
				Analysis_URL string
			}
			ReversingLabs struct {
				Threat_name     string
				Status          string
				First_seen      string
				Scanner_count   string
				Scanner_match   string
				Scanner_percent string
			}
			Spamhaus_HBL struct {
				Detection string
				Link      string
			}
			Triage struct {
				Malware_family string
				Score          string
				Link           string
				Tags           []string
				Signatures     []TriageSignature
			}
			UnpacMe []struct {
				Sha256_hash string
				Md5_hash    string
				Sha1_hash   string
				Detections  []string
				Link        string
			}
			VMRay struct{}
		}
		archive_pw string
	}
}
