package repository

import (
	"context"
	"fmt"

	"gitlab.com/mostwanted002/smaf/models"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func InitiateConnection(dbCreds models.DBCredentials) *mongo.Client {
	connect, err := mongo.Connect(context.TODO(), options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s", dbCreds.Username, dbCreds.Password, dbCreds.Host)))
	if err != nil {
		panic(err)
	}
	return connect
}

func PushToDB(sample models.Sample, client *mongo.Client) error {
	if sample.SampleTypeValue != "" {
		collection := client.Database("malware_samples").Collection(sample.SampleType)
		_, err := collection.InsertOne(context.TODO(), sample)
		if err != nil {
			return err
		}
	} else {
		collection := client.Database("malware_samples").Collection("Unknown")
		_, err := collection.InsertOne(context.TODO(), sample)
		if err != nil {
			return err
		}
	}
	return nil
}
