package modules

import (
	"crypto/md5"
	"crypto/sha256"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"

	"github.com/h2non/filetype"
	"gitlab.com/mostwanted002/smaf/constants"
	"gitlab.com/mostwanted002/smaf/models"
)

const BufferSize = 4096

func Identifier(file *os.File) models.Sample {
	fileInfo, _ := file.Stat()
	var filedata = []byte{}
	buffer := make([]byte, BufferSize)
	for {
		bytesRead, err := io.Reader.Read(file, buffer)
		if err != nil {
			if err != io.EOF {
				fmt.Printf("Error while reading sample:\n %f\n", err)
			}
			break
		}
		filedata = append(filedata, buffer[:bytesRead]...)
	}
	fmt.Printf("Bytes read: %d\n", len(filedata))
	kind, _ := filetype.Match(filedata)
	sha256sum := sha256.Sum256(filedata)
	md5sum := md5.Sum(filedata)
	thisSample := models.Sample{
		Sha256sum:       fmt.Sprintf("%x", sha256sum),
		Md5sum:          fmt.Sprintf("%x", md5sum),
		SampleSize:      fileInfo.Size(),
		SampleType:      kind.MIME.Type,
		SampleTypeValue: kind.MIME.Value,
		IsReported:      false,
		ReportURLs:      make([]string, 0),
		Timestamp:       fileInfo.ModTime().String(),
	}
	return thisSample
}

func IsReported(sample *models.Sample) bool {
	mbApiKey := os.Getenv("MALWARE_BAZAAR_API")

	if mbApiKey == "" {
		fmt.Println("Couldn't check for MalwareBazaar reports as MALWARE_BAZAAR_API environment variable is not set.")
	} else {
		query := url.Values{}
		query.Add("query", "get_info")
		query.Add("hash", sample.Sha256sum)

		resp, err := http.PostForm(constants.MALWARE_BAZAAR_API_URL, query)
		if err != nil {
			fmt.Printf("Error while querying MalwareBazaar API: %s\n", err)
			return false
		}
		mbResults := new(models.MalwareBazaarQueryResponse)
		json.NewDecoder(resp.Body).Decode(mbResults)
		if mbResults.Query_status != "ok" {
			return false
		}
		sample.ReportURLs = append(sample.ReportURLs, fmt.Sprintf("%s%s", constants.MALWARE_BAZAAR_REPORT_URL, sample.Sha256sum))
		sample.ReportURLs = append(sample.ReportURLs, mbResults.Data[0].Vendor_intel.ANYRUN.Analysis_URL)
		var reportUrl, _ = json.Marshal(mbResults.Data[0].Vendor_intel.CAPE)
		sample.ReportURLs = append(sample.ReportURLs, string(reportUrl))
		sample.ReportURLs = append(sample.ReportURLs, mbResults.Data[0].Vendor_intel.CERTPL_MWDB.Link)
		reportUrl, _ = json.Marshal(mbResults.Data[0].Vendor_intel.DocGuard)
		sample.ReportURLs = append(sample.ReportURLs, string(reportUrl))
		sample.ReportURLs = append(sample.ReportURLs, mbResults.Data[0].Vendor_intel.FileScanIO.Report_link)
		sample.ReportURLs = append(sample.ReportURLs, mbResults.Data[0].Vendor_intel.InQuestLabs.URL)
		sample.ReportURLs = append(sample.ReportURLs, mbResults.Data[0].Vendor_intel.Intezer.Analysis_URL)
		reportUrl, _ = json.Marshal(mbResults.Data[0].Vendor_intel.ReversingLabs)
		sample.ReportURLs = append(sample.ReportURLs, string(reportUrl))
		sample.ReportURLs = append(sample.ReportURLs, mbResults.Data[0].Vendor_intel.Spamhaus_HBL.Link)
		sample.ReportURLs = append(sample.ReportURLs, mbResults.Data[0].Vendor_intel.Triage.Link)
		reportUrl, _ = json.Marshal(mbResults.Data[0].Vendor_intel.VMRay)
		sample.ReportURLs = append(sample.ReportURLs, string(reportUrl))
		reportUrl, _ = json.Marshal(mbResults.Data[0].Vendor_intel.YOROI_YOMI)
		sample.ReportURLs = append(sample.ReportURLs, string(reportUrl))

		return true
	}

	return false
}
